# firefox-updater.sh

Shell script to update Fiirefox on Slackware or a Linux system running a Firefox outside its repositories.

This script grabs the latest version of Firefox and unarchives it into the ``/opt`` directory. It creates a symlink in ``/usr/local/bin`` and adds a ``firefox.desktop`` to ``/usr/share/applications`` if required.

Use the ``--local`` flag to install everything locally in your user directory.

Use the ``--remove`` flag to uninstall.

